<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PropertyCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $propertyId;
    public $path;
    /**
     * @var int
     */
    public $imageId;

    /**
     * Create a new event instance.
     * @param int $propertyId
     * @param string $path
     * @param int $imageId
     */
    public function __construct(int $propertyId, string $path, int $imageId)
    {
        $this->propertyId = $propertyId;
        $this->path = $path;
        $this->imageId = $imageId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
