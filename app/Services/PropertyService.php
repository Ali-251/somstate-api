<?php

namespace App\Services;

use App\Address;
use App\Events\PropertyCreated;
use App\Image;
use App\Property;
use App\Repositories\PropertyRepository;
use App\Services\storage\Exceptions\FileUploadException;
use App\Services\storage\LocalStorage;
use App\Services\storage\StorageGateway;
use Illuminate\Http\UploadedFile as httpFileUpload;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class PropertyService
{


    /**@var StorageGateway $storage */
    protected $storage;

    /**@var PropertyRepository $propertyRepository */
    protected $propertyRepository;

    public function __construct(LocalStorage $storage, PropertyRepository $propertyRepository)
    {
        $this->storage = $storage;
        $this->propertyRepository = $propertyRepository;
    }

    /**
     * @param $data
     * @return array
     * @throws \Exception
     */
    public function store($data): array
    {
        Log::debug(__METHOD__, $data);

        $data['user_id'] = 10;
        $user = Auth::user();
        if ($user) {
            $data['user_id'] = $user->id;
        }

        try {
            $property['description'] = $data['description'];
            $property['headline'] = $data['headline'];
            $property['price'] = $data['price'];
            $property['purpose'] = $data['purpose'];
            $property['type'] = $data['type'];
            $property['user_id'] = $data['user_id'];
            $property['bathrooms'] = $data['bathrooms'];
            $property['bedrooms'] = $data['bedrooms'];
            $property['carports'] = $data['carports'];

            $property = $this->propertyRepository->create($property);
            $propertyId = $property['id'];
            $propertyObject = Property::query()->where(['id' => $property['id']])->firstOrFail();
            $files = \request()->file('files');
            $allFiles = [];
            if ($files instanceof httpFileUpload) {
                $allFiles[] = $files;
            } else {
                $allFiles = $files;
            }
            $this->storePropertyImages($allFiles, $propertyObject);
            $this->storeAddress($data, $propertyId);

            return $property;
        } catch (\Throwable $e) {
            dd($e->getMessage());
            Log::error('Failed to create property ', $data);
            throw  $e;
        }
    }

    /**
     * @param array $files
     * @param Property $property
     * @throws \Exception
     */
    public function storePropertyImages(array $files, Property $property): void
    {
        foreach ($files as $file) {
            if (!$file instanceof UploadedFile) {
                Log::error('Invalid file type', [$file]);
                throw new \Exception("Invalid file upload");
            }

            $path = $this->storage->store($file);
            $image = new Image();
            $image->path = $path;
            $imageSave = $image->save();
            if (!$imageSave) {
                Log::error('failed to save image', $image->toArray());
                $ex = new FileUploadException("Failed to save property image");
                $ex->setUserFriendlyMessage("Failed to save property image");
            }
            try {
                $property->images()->save($image);
                event(new PropertyCreated($property->id, $path, $image->id));
            } catch (\Exception $e) {
                Log::error('failed to save property images (association)', [$e->getMessage()]);
            }
        }
    }

    public function storeAddress($data, $propertyId)
    {
        $add = new Address();
        $add->country = 'Somalia';
        $add->state = '';
        $add->city = $data['city'];
        $add->street_name = $data['street_name'];
        $add->street_number = $data['street_number'];
        $add->unit = $data['unit'];
        $add->property_id = $propertyId;

        return $add->save();
    }
}
