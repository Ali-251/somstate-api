<?php


namespace App\Services\storage;


use App\Services\storage\s3\S3StorageProvider;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class StorageGateway implements StorageInterface
{
    const STORAGE_PATH = 'uploads';
    /**
     * @var S3StorageProvider
     */
    private $storageProvider;

    public function __construct(LocalStorage $provider)
    {
        $this->storageProvider = $provider;
    }

    /**
     * @param UploadedFile $file
     * @return mixed
     */
    public function store(UploadedFile $file)
    {
        return $this->storageProvider->store($file);
    }

    public function delete(string $file)
    {
        return $this->storageProvider->delete($file);
    }
}
