<?php

namespace App\Services\storage\Exceptions;

use Throwable;

class FileUploadException extends \Exception
{

    protected $userFriendlyMessage = '';

    public function __construct($message = 'Failed to upload file', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return string
     */
    public function getUserFriendlyMessage(): string
    {
        return $this->userFriendlyMessage;
    }

    /**
     * @param string $userFriendlyMessage
     * @return FileUploadException
     */
    public function setUserFriendlyMessage(string $userFriendlyMessage): FileUploadException
    {
        $this->userFriendlyMessage = $userFriendlyMessage;

        return $this;
    }
}
