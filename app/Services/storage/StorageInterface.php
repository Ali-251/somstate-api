<?php

namespace App\Services\storage;


use Symfony\Component\HttpFoundation\File\File;

interface StorageInterface
{
    /**
     * @param File $file
     * @return mixed
     */
    public function store(File $file);

    public function delete(string $file);
}
