<?php


namespace App\Services\storage;


use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\File;

class LocalStorage implements StorageInterface
{
    /**
     * @var \Illuminate\Contracts\Filesystem\Filesystem
     */
    protected $storage;

    public function __construct()
    {
        $this->storage = Storage::disk('local');
    }

    /**
     * @param $path
     */
    public function get($path)
    {
        return new File(storage_path('/app/' . $path), true);
    }

    /**
     * @param File $file
     * @return mixed
     */
    public function store(File $file)
    {
        $fileName = time() . '.' . $file->getClientOriginalExtension();
        $path = sprintf("%s/%s", $this->getPath(), $fileName);
        $this->storage->put($path, file_get_contents($file));
        return $path;
    }

    public function delete(string $file)
    {
        return $this->storage->delete($file);
    }

    public static function getPath()
    {
        return 'uploads/original';
    }
}