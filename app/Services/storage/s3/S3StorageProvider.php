<?php

namespace App\Services\storage\s3;

use App\Services\storage\StorageInterface;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\File;

class S3StorageProvider implements StorageInterface
{
    /**
     * @var Filesystem
     */
    private $s3;
    /**
     * @var string
     */
    protected $storage_path;


    public function __construct(string $path = 'uploads/original')
    {
        $this->s3 = Storage::disk('s3');
        $this->storage_path = $path;
    }

    /**
     * @param File $file
     * @return mixed
     */
    public function store(File $file)
    {
        $fileName = time() . '.' . $file->getExtension();
        $path = sprintf("%s/%s", $this->storage_path, $fileName);
        $this->s3->put($path, file_get_contents($file), 'public');

        return $path;
    }

    public function delete(string $file)
    {
        return $this->s3->delete($file);
    }
}
