<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Laravel\Passport\Bridge\UserRepository;

class AuthController extends Controller
{

    protected $userRepository;

    const CODE_DUPLICATE_USER = '23000';

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function register(Request $request)
    {
        /*TODO move to requestForm validation*/
        $registrationFormData = $request->validate([
            'name' => 'required|max:55',
            'email' => 'required|email|min:5',
            'password' => 'required|between:6,255'
        ]);
        $registrationFormData['password'] = bcrypt($request->get('password'));

        try {
            //TODO catch exception and return proper errors
            $user = User::query()->create($registrationFormData);
            $accessToken = $user->createToken('authToken')->accessToken;
            return Response(['user' => $user, 'access_token' => $accessToken], 201);

        } catch (QueryException $q) {
            if ($q->getCode() === self::CODE_DUPLICATE_USER) {
                return Response(['message' => 'Email is taken'], 400);
            }

            throw $q;
        } catch (\Throwable $t) {
            Log::error('Failed to register user', [
                'data' => $registrationFormData,
                'exception' => $t->getMessage()
            ]);
            throw $t;
        }
    }

    public function login(Request $request)
    {
        $loginFormData = $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if (!Auth::attempt($loginFormData)) {
            return Response(['message' => 'Invalid credentials'], 401);
        }

        $accessToken = Auth::user()->createToken('authToken')->accessToken;

        return Response(['user' => Auth::user(), 'access_token' => $accessToken], 200);
    }

    public function logout(Request $request)
    {
        $token = $request->user()->token();
        $token->revoke();

        return response()->json(['You have been successfully logged out!'], 200);
    }
}
