<?php

namespace App\Http\Controllers\api\admin;

use App\Http\Controllers\Api\PropertySearchController;
use App\Http\Requests\Property\CreatePropertyRequest;
use App\Repositories\Admin\PropertyRepository;
use App\Services\PropertyService;
use App\Services\storage\LocalStorage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PropertyController extends PropertySearchController
{

    /**
     * @var \App\Repositories\Admin\PropertyRepository
     */

    protected $repository;

    /**
     * @var PropertyService $propertyService
     */
    private $propertyService;

    public function __construct(PropertyRepository $repos, PropertyService $service)
    {
        parent::__construct($repos);

        $this->propertyService = $service;
    }

    /**
     * @param Request $request
     * @param CreatePropertyRequest $createPropertyRequest
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function create(Request $request, CreatePropertyRequest $createPropertyRequest)
    {
//        $fileName = 'uploads/original/1617408380.jpg';
//        $storage = new LocalStorage();
//        $data = $storage->get($fileName);
//        dd($data);
        try {
            return $this->propertyService->store($request->all());
        } catch (\Throwable $t) {
            Log::error("Failed to create property", [$t]);
            return response()->json(['message' => 'internal server error'], 500);
        } catch (\Exception $e) {
            Log::error("Failed to create property", [$e]);
            Log::error("Failed to create property with data", [$request->all()]);
            return response()->json(['message' => 'internal server error'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $result = $this->repository->delete($id);
        $statusCode = 200;

        if (!$result) {
            $statusCode = 403;
        }
        return \response()->json(['deleted' => $result], $statusCode);
    }
}
