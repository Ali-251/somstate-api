<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Property;
use App\Repositories\RelatedPropertyRepository;

class RelatedPropertyController extends Controller
{

    /**
     * @var \App\Repositories\RelatedPropertyRepository
     */
    private $repository;

    public function __construct(RelatedPropertyRepository $repos)
    {
        $this->repository = $repos ?: new RelatedPropertyRepository(new Property());
    }

    public function all($id)
    {
        $filters = [['id', '!=', $id]];
        $result = $this->repository->related($filters, ['images', 'address']);

        return response($result, 200);
    }
}
