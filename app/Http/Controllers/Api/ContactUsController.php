<?php

namespace App\Http\Controllers\api;

use App\ContactUs;
use App\Http\Controllers\Controller;
use App\Repositories\ContactUsRepository;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    /**
     * @var \App\Repositories\MessagesRepository
     */
    protected $repository;

    public function __construct(ContactUs $model)
    {
        $this->repository = new ContactUsRepository($model);
    }

    public function create(Request $request)
    {
        $this->validate(
            $request,
            [
                'name' => 'string|required',
                'email' => 'string|required',
                'company' => 'string',
                'phone' => 'string',
                'message' => 'string|required|min:50|max:1000',
            ]
        );

        $data = $request->only(['message', 'phone', 'email', 'name', 'property_id']);
        $result = $this->repository->create($data);
        return response()->json($result, '201');

    }

}
