<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Property;
use App\Repositories\PropertyRepository;
use Illuminate\Http\Request;

class PropertySearchController extends Controller
{
    /**
     * @var PropertyRepository
     */
    protected $repository;

    public function __construct(PropertyRepository $repos)
    {
        $this->repository = $repos ?: new PropertyRepository(new Property());
    }

    public function index()
    {
        $result = $this->repository->all([], $this->repository::RELATIONSHIPS);

        return response($result, 200);
    }

    public function search(Request $request)
    {
        $result = $this->repository->filteredSearch($request->get('city'), $request->get('purpose'));

        return response($result, 200);
    }

    public function get($id)
    {
        $result = $this->repository->get($id, [], $this->repository::RELATIONSHIPS);

        return response($result, 200);
    }

    public function delete($id)
    {
        $result = $this->repository->delete($id);

        return response(['success' => $result], 200);
    }

    public function update($id, Request $request)
    {
        $data = $request->all();
        $this->repository->update($id, $data);

        return response('ok', 200);
    }
}
