<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Messages;
use App\Repositories\MessagesRepository;
use Illuminate\Http\Request;

class MessagesController extends Controller
{

    /**
     * @var \App\Repositories\MessagesRepository
     */
    protected $repository;

    public function __construct(Messages $model)
    {
        $this->repository = new MessagesRepository($model);
    }

    public function all()
    {
        return response()->json($messages = $this->repository->all(), '200');
    }

    public function create(Request $request)
    {
        $this->validate(
            $request,
            [
                'property_id' => 'integer|required',
                'message' => 'string|required',
                'name' => 'string|required',
                'phone' => 'string|required',
                'email' => 'string|required',
            ]
        );

        $data = $request->only(['message', 'phone', 'email', 'name', 'property_id']);
        $result = $this->repository->create($data);
        return response()->json($result, '201');
    }
}
