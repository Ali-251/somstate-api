<?php

namespace App\Http\Middleware;

use Closure;

class CorsHandler
{
    const WHITE_LIST = [
        "https://focuslrealestate.com",
         "http://localhost:9090",
        "https://www.focuslrealestate.com",
    ];
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request)
            ->header('Access-Control-Allow-Origin', "*")
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
            ->header('Access-Control-Allow-Headers', ' Origin, Content-Type, Accept, Authorization, X-Request-With')
            ->header('Access-Control-Allow-Credentials', ' true');
    }

    public function getAllowedDomainName()
    {
        $requestedUrl = request()->server('HTTP_ORIGIN');
        return in_array($requestedUrl, self::WHITE_LIST, true) ? $requestedUrl: false;
    }
}
