<?php

namespace App\Http\Requests\Property;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreatePropertyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "description" => 'required|min:5',
            "headline" => "string|required",
            "price" => "required|int|min:1000",
            "purpose" => [
                'required',
                Rule::in('Rent', 'Sale', 'Investment'),
            ],

            "type" => [
                'required',
                Rule::in('House', 'Apartment', 'Land', 'Other'),
            ],
            "bathrooms" => "int|",
            "bedrooms" => "int|",
            "carports" => "int|",
        ];
    }

    public function messages()
    {
        return [
            'type.required' => 'property type is required',
            'purpose.required' => 'property purpose is required',
            'headline.required' => 'property headline is required',
            'headline.string' => 'property headline is required',
        ];
    }
}
