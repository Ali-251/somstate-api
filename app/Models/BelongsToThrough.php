<?php

namespace App\Models;

trait BelongsToThrough
{
    public function initializeBelongsToThrough()
    {
        $this->hidden[] = 'laravel_through_key';
        $this->hidden[] = 'pivot';
    }
}
