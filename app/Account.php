<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public function agents()
    {
        return $this->hasMany(User::class);
    }

    public function properties()
    {
        return $this->hasMany(Property::class);
    }
}
