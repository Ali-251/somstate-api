<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
    protected $fillable = [

        'message',
        'name',
        'phone',
        'email',
        'property_id'
    ];
}
