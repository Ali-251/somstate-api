<?php

namespace App\Listeners;

use App\Image;
use App\Property;
use App\Services\storage\LocalStorage;
use App\Services\storage\s3\S3StorageProvider;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ResizePropertyImages implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /**
     * @var S3StorageProvider
     */
    private $s3StorageProvider;
    /**
     * @var LocalStorage
     */
    private $localStorage;

    public function __construct(S3StorageProvider $s3StorageProvider, LocalStorage $localStorage)
    {
        $this->s3StorageProvider = $s3StorageProvider;
        $this->localStorage = $localStorage;
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle($event)
    {
        $filePath = $event->path;
        $imageId = $event->imageId;
        $propertyId = $event->propertyId;

        logger('started processing creating images for property ' . $propertyId, [
            'propertyId' => $propertyId,
            'image_path' => $filePath,
        ]);
        $file = $this->localStorage->get($filePath);
        $property = Property::query()->where(['id' => $propertyId])->first();
        $path = $this->s3StorageProvider->store($file);
        $image = new Image();
        $image->path = $path;
        $image->is_cloud = true;
        $result = $image->save();
        if ($result) {
            $property->images()->save($image);
            $image->newQuery()->where(['id' => $imageId])->delete();
            $this->localStorage->delete($filePath);
        }
    }
}

