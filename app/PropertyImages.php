<?php

namespace App;

use App\Models\BelongsToThrough;
use Illuminate\Database\Eloquent\Model;

class PropertyImages extends Model
{
    use BelongsToThrough;

    protected $fillable = [
        'property_id',
        'image_id'
    ];
}
