<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $guarded = ['id'];
    public function agent()
    {
        return $this->hasOne(User::class, 'id');
    }

    public function address()
    {
        return $this->hasOne(Address::class);
    }

    public function images()
    {
        return $this->belongsToMany(Image::class, PropertyImages::class, 'property_id', 'image_id');
    }

    public function features()
    {
        return $this->belongsToMany(Feature::class, PropertyFeatures::class, 'property_id', 'feature_id');
    }
}
