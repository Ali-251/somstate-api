<?php

namespace App;

use App\Models\BelongsToThrough;
use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    use BelongsToThrough;

    public function property()
    {
        return $this->belongsToMany(Property::class, 'property_features');
    }
}
