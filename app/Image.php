<?php

namespace App;

use App\Models\BelongsToThrough;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use BelongsToThrough;

    protected $fillable = [
        'path',
    ];
    protected $casts = [
        'created_at' => 'datetime:d-m-Y',
        'modified_at' => 'datetime:d-m-Y',
    ];

    public function property()
    {
        return $this->belongsTo(Property::class);
    }
}
