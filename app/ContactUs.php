<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    protected $fillable = [

        'message',
        'company',
        'phone',
        'email',
        'name'
    ];
}
