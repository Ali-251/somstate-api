<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface RepositoryInterface
{

    /**
     * @param $id
     * @param array $filter
     * @param array $relationships
     * @return array
     */
    public function get($id, array $filter = [], $relationships = []): array;

    /**
     * @param array $filter
     * @param array $relationships
     * @return array
     */
    public function all(array $filter = [], $relationships = []): array;

    /**
     * @param int $id
     * @param array $data
     * @return int
     */
    public function update(int $id, array $data): int;


    /**
     * @param array $data
     * @return array
     */
    public function create(array $data): array;

    /**
     * @param $id
     * @return bool
     */
    public function delete($id): bool;
}
