<?php

namespace App\Repository;

use App\Repositories\AbstractRepository;
use App\User;

class UserRepository extends AbstractRepository
{
    public function __construct(User $model)
    {
        if ($model === null) {
            $model = new User();
        }

        parent::__construct($model);
    }

}
