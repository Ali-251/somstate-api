<?php

namespace App\Repositories;

use App\Property;

class PropertyRepository extends AbstractRepository
{

    const RELATIONSHIPS = [
        'images', 'features', 'address', 'agent'
    ];

    public function __construct(Property $model)
    {
        parent::__construct($model);
    }

    public function get($id, array $filter = [], $relationships = []): array
    {
        $query = $this->model::query()->where([['id', '=', $id]]);

        $query = $this->withRelationship($query, $relationships);

        return $this->withFilters($query, $filter)->firstOrFail()->toArray();
    }

    public function filteredSearch(string $city = null, string $purpose = null): array
    {
        $query = $this->model::query();
        $result = $this->withRelationship($query, self::RELATIONSHIPS)->get();

        if (empty($city) && empty($purpose)) {
            return $result->toArray();
        }

        if (!empty($city)) {
            $result = $result->filter(function ($item) use ($city) {
                if (!$item->address) {
                    return false;
                }
                return strtolower($item->address->city) === strtolower($city);
            });
        }


        if (!empty($purpose)) {
            $result = $result->filter(function ($item) use ($purpose) {
                return strtolower($item->purpose) === strtolower($purpose);
            });
        }

        return $result->flatten()->toArray();
    }
}
