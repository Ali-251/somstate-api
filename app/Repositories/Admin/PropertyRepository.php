<?php

namespace App\Repositories\Admin;

use Illuminate\Support\Facades\Auth;

class PropertyRepository extends \App\Repositories\PropertyRepository
{
    /**
     * @var \App\Repositories\Admin\PropertyRepository
     */
    protected $repository;

    public function delete($id): bool
    {
        $record = $this->model::query()->where([['id', '=', $id]])->first();

        if (!$record) {
            return false;
        }

        if (Auth::user()->id !== $record->user_id) {
            return false;
        }

        if ($record->delete()) {
            return true;
        }

        return false;
    }
}
