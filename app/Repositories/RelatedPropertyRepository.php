<?php

namespace App\Repositories;

class RelatedPropertyRepository extends PropertyRepository
{

    const LIMIT = 5;
    public function related(array $filter = [], $relationships = []): array
    {
        //TODO put logic here to find related property
        $data = $this->model::query()->with($relationships)
            ->limit(self::LIMIT)
            ->get()
            ->toArray();

        return $data;
    }
}
