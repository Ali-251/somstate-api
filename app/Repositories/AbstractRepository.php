<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

abstract class AbstractRepository implements RepositoryInterface
{

    /**
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }


    public function get($id, array $filter = [], $relationships = []): array
    {
        return $this->model::query()->where(['id' => $id])->firstOrFail()->toArray();
    }

    public function all(array $filter = [], $relationships = []): array
    {
        $query = $this->model::query();

        $query = $this->withRelationship($query, $relationships);

        return $this->withFilters($query, $filter)->get()->toArray();
    }


    /**
     * @param int $id
     * @param array $data
     * @return int
     */
    public function update(int $id, array $data): int
    {
        return $this->model::query()->update($data);
    }

    /**
     * @param array $data
     * @return array
     */
    public function create(array $data): array
    {
        return $this->model::query()->create($data)->toArray();
    }

    /**
     * @param $id
     * @return bool
     * @throws \Exception
     */
    public function delete($id): bool
    {
        $record = $this->model::query()->where([['id', '=', $id]])->first();
        if (!$record) {
            return false;
        }

        if ($record->delete()) {
            return true;
        }
        return false;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array $relationShips
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function withRelationship(Builder $query, array $relationShips = []): Builder
    {
        if (empty($relationShips)) {
            return $query;
        }

        return $query->with($relationShips);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array $filters
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function withFilters(Builder $query, array $filters = []): Builder
    {
        if (empty($filters)) {
            return $query;
        }

        return $query->where($filters);
    }
}
