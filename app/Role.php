<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const CLIENT = 6;
    const ADMIN = 4;
    const SUPER_ADMIN = 3;
    const AGENT = 1;
}
