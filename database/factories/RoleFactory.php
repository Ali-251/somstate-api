<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Role;
use Faker\Generator as Faker;

$factory->define(Role::class, function (Faker $faker) {
    $role = $faker->randomElement(['Agent', 'user', 'Admin', 'super_Admin', 'client']);
    return [
        'name' => strtoupper($role),
        'description' => $role,
    ];
});
