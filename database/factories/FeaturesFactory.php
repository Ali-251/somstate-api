<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Feature;
use Faker\Generator as Faker;

$factory->define(Feature::class, function (Faker $faker) {
    return [
        'name' => $faker->randomElement(['rooms', 'toilets', 'carports' ]),
        'value' => $faker->numberBetween(0,4),
    ];
});
