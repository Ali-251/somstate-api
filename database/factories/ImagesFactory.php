<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Image;
use Faker\Generator as Faker;

$factory->define(Image::class, function (Faker $faker) {
    return [
        'path' => $faker->randomElement(['/featured.jpg', '/featured-2.jpeg', '/featured.jpg', '/featured-2.jpeg']),
    ];
});
