<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Property;
use Faker\Generator as Faker;

const DESCRIPTION = "houses for Sale in Somalia or land for Sale in Somalia. House for rent in Somalia. You can find all in this website. Find your dream property with ease. Site is in development - COMING SOON";

$factory->define(Property::class, function (Faker $faker) {
    return [
        'headline' => $faker->randomElement(['Amazing home for sale', 'Luxury home for rent', 'Five Star home']),
        'description' => DESCRIPTION,
        'purpose' => $faker->randomElement(['BUY', 'RENT', 'INVESTMENT']),
        'type' => $faker->randomElement(['Unit', 'House', 'Other']),
        'price' => $faker->numberBetween(150000, 200000),
        'user_id' => factory(\App\User::class),
        'bathrooms' => $faker->randomDigit,
        'bedrooms' => $faker->randomDigit,
        'carports' => $faker->randomDigit,

    ];
});
