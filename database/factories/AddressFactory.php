<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Address;
use Faker\Generator as Faker;


$cities = [
    'Mogadishu',
    'Hargeisa',
    'Bosaso',
    'Kismayo',
    'Baydhabo',
    'Galkacayo',
    'Burco',
    'Garowe',
    'laascaanood',
    'Jowhaar',
    'Berbera',
    'Ceerigaabo',
    'Beledweyne',
];


$factory->define(Address::class, function (Faker $faker) use($cities) {
    $cityAndState =  $faker->randomElement($cities);
    return [
        'state' => $cityAndState,
        'city' => $cityAndState,
        'country' =>'Somalia',
        'street_name' => $faker->streetName,
        'street_number' => $faker->numberBetween(1,10000),
        'unit' => $faker->numberBetween(1,100),
    ];

});


