<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_images', function (Blueprint $table) {
            $table->foreignId('image_id')->references('id')->on('images')->onUpdate('cascade')->cascadeOnDelete();
            $table->foreignId('property_id')->references('id')->on('properties')->onUpdate('cascade')->cascadeOnDelete();

            $table->unique(['image_id', 'property_id']);
            $table->timestamps();
            $table->primary(['image_id', 'property_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_images');
    }
}
