<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('properties', function (Blueprint $table) {
            $date = \Carbon\Carbon::now()->addDays(30);
            $table->id();

            $table->string('headline');
            $table->text('description');
            $table->string('purpose');
            $table->string('type');
            $table->bigInteger('price');
            $table->smallInteger('bathrooms');
            $table->smallInteger('bedrooms');
            $table->smallInteger('carports');
            $table->timestamp('active')->default($date);
            $table->boolean('draft')->default(1);
            $table->bigInteger('view')->default(0);
            $table->foreignId('user_id')->references('id')->on('users');
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
