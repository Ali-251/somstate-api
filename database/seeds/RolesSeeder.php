<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(\App\Role::class, 2)->make()->each(function (\App\Role $role) {

            try {
                $role->save();
            } catch (Exception $e) {

            }
        });
    }
}
