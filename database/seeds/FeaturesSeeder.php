<?php

use Illuminate\Database\Seeder;

class FeaturesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = 0;
        while ($count< 10) {
            try {
                factory(\App\Feature::class, 3)->create();
            }
            catch (\Exception $e) {}
            $count++;
        }
    }
}
