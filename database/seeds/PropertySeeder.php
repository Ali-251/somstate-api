<?php

use App\Feature;
use Illuminate\Database\Seeder;

class PropertySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Property::class, 20)->create();

        \App\Property::all()->each(function (\App\Property $p) {
            $features = Feature::all();
            $p->features()->saveMany($features);

            $images = \App\Image::all();
            $p->images()->saveMany($images);
            $p->address()->save(factory(\App\Address::class)->make());
        });

    }
}
