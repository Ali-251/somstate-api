<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
/*======password reset======= */
Route::post('/password/email', 'Api\Auth\ForgotPasswordController@sendResetLinkEmail');
Route::post('/password/reset', 'Api\Auth\ResetPasswordController@reset');
// public routes
Route::post('/v1/auth/login', 'Api\Auth\AuthController@login')->name('login.Api');
Route::post('/v1/auth/register', 'Api\Auth\AuthController@register')->name('register.Api');
Route::post('/v1/auth/password/email', 'Api\Auth\ForgotPasswordController@sendResetLinkEmail');
Route::post('/v1/auth/password/reset', 'Api\Auth\ResetPasswordController@reset');

Route::middleware([\App\Http\Middleware\CorsHandler::class, 'auth:api'])->group(function () {
    // messages
    Route::get('v1/admin/messages', 'Api\MessagesController@all');
    Route::post('/v1/auth/logout', 'Api\Auth\AuthController@logout')->name('logout');

    // properties
    Route::get('/v1/admin/property', 'Api\admin\PropertyController@index');
    Route::delete("/v1/admin/property/{id}", 'Api\admin\PropertyController@destroy');
    Route::post('/v1/admin/property', 'Api\admin\PropertyController@create');

});

///=======public property routes=========
Route::get('/v1/property', 'Api\PropertySearchController@index');
Route::post('/v1/property', 'Api\PropertySearchController@search');
Route::get('/v1/property/{id}', 'Api\PropertySearchController@get');
Route::get('/v1/related/{id}', 'Api\RelatedPropertyController@all');

///=====contact form=====
Route::post('v1/messages', 'Api\MessagesController@create');

//contact us
Route::post('v1/contact-us', 'Api\ContactUsController@create');

