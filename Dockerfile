FROM php:7.4-fpm-alpine

WORKDIR /var/www
RUN docker-php-ext-enable sodium
RUN docker-php-ext-install pdo_mysql

RUN apk add libsodium-dev
RUN docker-php-ext-install sodium

 RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
 COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composerp

# Copy config
ADD ./docker/php/local.ini /usr/local/etc/php/php.ini
COPY . /var/www
RUN addgroup -g 1000 -S www && \
    adduser -u 1000 -S www -G www
USER www
COPY --chown=www:www . /var/www
RUN chmod -R 775 /var/www/storage

RUN ["chmod", "+x", "./docker/php/start_script.sh"]
EXPOSE 9000
CMD ./docker/php/start_script.sh
