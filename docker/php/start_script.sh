#!/bin/sh

# turn on bash's job control
set -m

echo "=========Application entry point======="

# Start the primary process and put it in the background
php-fpm &

echo "==========Generating keys and running db migrations =========";
php artisan passport:install --no-interaction
php artisan passport:keys --no-interaction -vvv
php artisan config:clear
php artisan queue:work

# now we bring the primary process back into the foreground
# and leave it there
fg %1
